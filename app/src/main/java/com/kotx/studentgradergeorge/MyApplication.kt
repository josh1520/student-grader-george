package com.kotx.studentgradergeorge

import android.app.Application
import com.kotx.studentgradergeorge.domain.MainRepository
import com.kotx.studengradergeorge.domain.database.DatabaseStructure
import kotlinx.coroutines.*

class MyApplication : Application() {
    val applicationScope = CoroutineScope(SupervisorJob())

    private val databaseStructure by lazy { DatabaseStructure.getDatabase(this) }
    val repository by lazy {
        MainRepository(
            databaseStructure.studentDau(),
            databaseStructure.courseDau(),
            databaseStructure.gradeDau()
        )
    }

}