package com.kotx.studentgradergeorge.data.model

import androidx.room.*
import androidx.room.ForeignKey.CASCADE
import java.util.*


@Entity(
    tableName = "student_table", foreignKeys = [ForeignKey(
        entity = Course::class,
        parentColumns = arrayOf("courseId"),
        childColumns = arrayOf("course_id"),
        onDelete = CASCADE
    )]
)
data class Student(
    @PrimaryKey val studentId: String = UUID.randomUUID().toString(),
    @ColumnInfo(name = "first_name") var firstname: String,
    @ColumnInfo(name = "last_name") var lastname: String,
    var email: String,
    val course_id: String

)


@Entity(
    tableName = "course_table", foreignKeys = [ForeignKey(
        entity = Student::class,
        parentColumns = arrayOf("studentId"),
        childColumns = arrayOf("student_id"),
        onDelete = CASCADE
    )]
)
data class Course(
    @PrimaryKey val courseId: String = UUID.randomUUID().toString(),
    val student_id: String,
    var name: String,
    var lecture: String
)

@Entity(tableName = "grade_table")
data class Grade(
    @PrimaryKey val gradeId: String = UUID.randomUUID().toString(),
    val student_id: String,
    val course_id: String,
    val grade: Byte,
    var timestamp: Long = System.currentTimeMillis()
)

// joining fun's
@Entity(primaryKeys = ["studentId", "courseId"])
data class StudentsWithCoursers(val studentId: String, val courseId: String)

@Entity(primaryKeys = ["courseId", "gradeId"])
data class CoursersWithGrades(val courseId: String, val gradeId: String)

//relations3 Grades n->1 Course

//relations1  Students 1->n Courses  1->n Grades
data class StudentAndCoursesAndGrades(
    @Embedded val student: Student,
    @Relation(
        parentColumn = "studentId", entityColumn = "student_id", associateBy = Junction(
            StudentsWithCoursers::class
        )
    ) val courses: List<Course>,
    @Relation(
        parentColumn = "courseId", entityColumn = "course_id", associateBy = Junction(
            CoursersWithGrades::class
        )
    ) val grades: List<Grade>,
)
//relations2 Course 1->n Students 1->n Grades
//relations4 Grades n->1 Student


