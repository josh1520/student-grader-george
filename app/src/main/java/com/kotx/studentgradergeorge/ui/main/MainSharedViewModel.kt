package com.kotx.studentgradergeorge.ui.main

import androidx.lifecycle.*
import com.kotx.studentgradergeorge.data.model.Course
import com.kotx.studentgradergeorge.data.model.Grade
import com.kotx.studentgradergeorge.data.model.Student
import com.kotx.studentgradergeorge.data.model.StudentAndCoursesAndGrades
import com.kotx.studentgradergeorge.domain.MainRepository
import kotlinx.coroutines.launch

class MainSharedViewModel(private val repository: MainRepository) : ViewModel() {
    //get fun's
    val allStudents: LiveData<List<Student>> = repository.allStudents.asLiveData()
    val allStudentsWithCoursers: LiveData<List<StudentAndCoursesAndGrades>> =
        repository.allStudentsWithCoursers.asLiveData()
    val allCoursers: LiveData<List<Course>> = repository.allCoursers.asLiveData()
    val allGrades: LiveData<List<Grade>> = repository.allGrades.asLiveData()
    //inserts fun's

    fun insertStudent(vararg student: Student) = viewModelScope.launch {
        repository.insertStudent(*student)
    }

    fun insertCourse(vararg course: Course) = viewModelScope.launch {
        repository.insertCourse(*course)
    }

    fun insertGrade(vararg grade: Grade) = viewModelScope.launch {
        repository.insertGrade(*grade)
    }

    //update fun's
    fun updateStudent(vararg student: Student) = viewModelScope.launch {
        repository.updateStudent(*student)
    }

    fun updateCourse(vararg course: Course) = viewModelScope.launch {
        repository.updateCourse(*course)
    }

    suspend fun updateGrade(vararg grade: Grade) = viewModelScope.launch {
        repository.updateGrade(*grade)
    }

    //delete fun's
    fun deleteStudent(student: Student) = viewModelScope.launch {
        repository.deleteStudent(student)
    }

    fun deleteAllStudents() = viewModelScope.launch {
        repository.deleteAllStudents()
    }

    fun deleteCourse(course: Course) = viewModelScope.launch {
        repository.deleteCourse(course)
    }

    fun deleteAllCourses() = viewModelScope.launch {
        repository.deleteAllCourses()

    }

    fun deleteGrade(grade: Grade) = viewModelScope.launch {
        repository.deleteGrade(grade)
    }

    fun deleteAllGrades() = viewModelScope.launch {
        repository.deleteAllGrades()

    }
}
    class WordViewModelFactory(private val repository: MainRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            if (modelClass.isAssignableFrom(MainSharedViewModel::class.java)) {
                @Suppress("UNCHECKED_CAST")
                return MainSharedViewModel(repository) as T
            }
            throw IllegalArgumentException("Unknown ViewModel class")
        }
    }