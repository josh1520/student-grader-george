package com.kotx.studengradergeorge.domain.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.kotx.studentgradergeorge.data.model.*
import com.kotx.studentgradergeorge.domain.database.CourseDao
import com.kotx.studentgradergeorge.domain.database.GradeDau
import com.kotx.studentgradergeorge.domain.database.StudentDao

@Database(
    entities = [Student::class, Grade::class, Course::class,
        StudentsWithCoursers::class, CoursersWithGrades::class],
    version = 1
)
abstract class DatabaseStructure : RoomDatabase() {
    abstract fun studentDau(): StudentDao
    abstract fun gradeDau(): GradeDau
    abstract fun courseDau(): CourseDao

    companion object {
        var INSTANCE: DatabaseStructure? = null
        fun getDatabase(context: Context): DatabaseStructure {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    DatabaseStructure::class.java,
                    DatabaseStructure::javaClass.name
                ).build()
                INSTANCE = instance
                instance

            }
        }
    }
}