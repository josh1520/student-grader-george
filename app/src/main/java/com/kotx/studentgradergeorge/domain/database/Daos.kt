package com.kotx.studentgradergeorge.domain.database

import androidx.room.*
import com.kotx.studentgradergeorge.data.model.Course
import com.kotx.studentgradergeorge.data.model.Grade
import com.kotx.studentgradergeorge.data.model.Student
import com.kotx.studentgradergeorge.data.model.StudentAndCoursesAndGrades
import kotlinx.coroutines.flow.Flow

sealed interface CourseDao {
    @Query("select * from course_table order by name asc")
     fun getAllCourses(): Flow<List<Course>>

    @Insert
    suspend fun addCourse(vararg course: Course)

    @Update
    suspend fun updateCourses(vararg courses: Course)

    @Delete
    suspend fun deleteCourse(course: Course)

    @Query("DELETE FROM course_table")
     fun deleteAllCourses()
}


@Dao
sealed interface StudentDao {
    @Transaction
    @Query("select * from student_table")
     fun getAllStudentsWithCourses():Flow<List<StudentAndCoursesAndGrades>>
    @Query("select * from student_table order by first_name asc")
     fun getAllStudents(): Flow<List<Student>>

    @Insert
    suspend fun addStudent(vararg student: Student)

    @Update
    suspend fun updateStudents(vararg student: Student)

    @Delete
    suspend fun deleteStudent(student: Student)

    @Query("DELETE FROM student_table")
     fun deleteAllStudents()
}

sealed interface GradeDau {
    @Query("select * from grade_table order by grade asc")
     fun getAllGrades(): Flow<List<Grade>>

    @Insert
    suspend fun addGrade(vararg grade: Grade)

    @Update
    suspend fun updateGrades(vararg grade: Grade)

    @Delete
    suspend fun deleteGrade(grade: Grade)

    @Query("DELETE FROM grade_table")
     fun deleteAllGrades()
}