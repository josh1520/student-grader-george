package com.kotx.studentgradergeorge.domain

import com.kotx.studentgradergeorge.data.model.Course
import com.kotx.studentgradergeorge.data.model.Grade
import com.kotx.studentgradergeorge.data.model.Student
import com.kotx.studentgradergeorge.domain.database.CourseDao
import com.kotx.studentgradergeorge.domain.database.GradeDau
import com.kotx.studentgradergeorge.domain.database.StudentDao

class MainRepository(
    private val studentDao: StudentDao,
    private val courseDao: CourseDao,
    private val gradeDau: GradeDau
) {

    //get fun's
    val allStudents by lazy { studentDao.getAllStudents() }
    val allStudentsWithCoursers by lazy { studentDao.getAllStudentsWithCourses() }
    val allCoursers by lazy { courseDao.getAllCourses() }
    val allGrades by lazy { gradeDau.getAllGrades() }


    //inserts fun's
    suspend fun insertStudent(vararg student: Student) = studentDao.addStudent(*student)
    suspend fun insertCourse(vararg course: Course) = courseDao.addCourse(*course)
    suspend fun insertGrade(vararg grade: Grade) = gradeDau.addGrade(*grade)

    //update fun's
    suspend fun updateStudent(vararg student: Student) = studentDao.updateStudents(*student)
    suspend fun updateCourse(vararg course: Course) = courseDao.updateCourses(*course)
    suspend fun updateGrade(vararg grade: Grade) = gradeDau.updateGrades(*grade)


    //delete fun's
    suspend fun deleteStudent(student: Student) = studentDao.deleteStudent(student)
    suspend fun deleteAllStudents() = studentDao.deleteAllStudents()

    suspend fun deleteCourse(course: Course) = courseDao.deleteCourse(course)
    suspend fun deleteAllCourses() = courseDao.deleteAllCourses()

    suspend fun deleteGrade(grade: Grade) = gradeDau.deleteGrade(grade)
    suspend fun deleteAllGrades() = gradeDau.deleteAllGrades()

}